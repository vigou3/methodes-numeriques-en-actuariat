%%% Copyright (C) 2019-2025 Vincent Goulet
%%%
%%% Ce fichier fait partie du projet «ACT-2002 Méthodes numériques en
%%% actuariat»
%%% https://gitlab.com/vigou3/methodes-numeriques-en-actuariat
%%%
%%% Cette création est mise à disposition sous licence
%%% Attribution-Partage dans les mêmes conditions 4.0
%%% International de Creative Commons.
%%% https://creativecommons.org/licenses/by-sa/4.0/

\section{Présentation du cours}

\begin{frame}[plain]
  \centering
  \includegraphics[width=0.9\linewidth]{images/meteo}
\end{frame}

\begin{frame}[plain]
  \centering\large
  19 janvier 2038 3 h 14 min 8 sec UTC \\
  \bigskip
  \pause
  \emph{\textcolor{rouge}{Épocalypse}} \\
  \pause
  Bogue de l'an 2038
\end{frame}

\begin{frame}[plain]
  \centering\large
  \textbf{Temps Unix} \\
  Nombre de secondes depuis le \\
  1{\ier} janvier 1970, 0 h 00 min 0 sec \\
  \bigskip
  \pause
  Entier \alert{signé} de 32~bits \\
  \bigskip
  \pause
  Maximum \\
  $2^{31} − 1 = \np{2 147 483 647}$ secondes
\end{frame}

\begin{frame}[plain]
  \large
  \begin{tabbing}
    Représentation binaire \quad\=\kill
    Date réelle \> 19 janvier 2038 3 h 14 min
    \only<1>{5}\only<2>{6}\only<3>{7}\only<4>{8} s \\
    Représentation binaire \>
    \only<1-3>{01111111 11111111 11111111}\only<4>{10000000 00000000 00000000}
    \only<1>{11111101}\only<2>{11111110}\only<3>{11111111}\only<4>{00000000} \\
    Valeur décimale \>
    \only<1>{\np{2 147 483 645}}\only<2>{\np{2 147 483 646}}\only<3>{\np{2 147 483 647}}\only<4>{$-\np{2 147 483 648}$} \\
    Date Unix \>
    \only<1-3>{19 janvier 2038 3 h 14 min}\only<4>{13 décembre 1901 20 h 45 min}
    \only<1>{5}\only<2>{6}\only<3>{7}\only<4>{52} s
  \end{tabbing}
\end{frame}

\begin{frame}[plain]
  \centering
  \includegraphics[width=0.8\textwidth,keepaspectratio]{images/y2k_and_2038}
\end{frame}

\begin{frame}[plain]
  \begin{textblock*}{\paperwidth}(0mm,0mm)
    \includegraphics[width=\paperwidth,keepaspectratio]{images/patriot}
  \end{textblock*}
\end{frame}

\begin{frame}[plain]
  \begin{textblock*}{0.9\paperwidth}(0.05\paperwidth,7mm)
    \includegraphics[width=\linewidth,keepaspectratio]{images/pca}
  \end{textblock*}

\end{frame}

\begin{frame}
  \frametitle{Recherche reproductible}

  «Un travail de recherche est dit reproductible si toutes les
  informations qui concernent ce travail, incluant, sans s’y limiter, le
  texte, les données, et le code de programmation, sont rendues
  disponibles de telle sorte que n’importe quel chercheur indépendant
  peut reproduire les résultats.»

  (\href{https://doi.org/10.1109/MSP.2009.932122}{Vandewalle,
    Kovacevic et Vetterli, 2009}; %
  traduction de \href{https://rr-france.github.io/bookrr}{Desquilbet
    et~collab, 2019})
\end{frame}

\begin{frame}
  \frametitle{Pyramide de la communication en science des données}
  \centering

  \setlength{\unitlength}{7mm}
  \begin{picture}(12,9)
    \thicklines
    {
      \color{LightBlue2}
      \polygon*(0,0)  (0.9,1.35)(11.1,1.35)(12,0)
      \polygon*(1,1.5)(1.9,2.85)(10.1,2.85)(11,1.5)
      \polygon*(2,3.0)(2.9,4.35)( 9.1,4.35)(10,3.0)
      \polygon*(3,4.5)(3.9,5.85)( 8.1,5.85)( 9,4.5)
      \polygon*(4,6.0)(4.9,7.35)( 7.1,7.35)( 8,6.0)
      \polygon*(5,7.5)(6,9)(7,7.5)
    }

    \polygon(0,0)  (0.9,1.35)(11.1,1.35)(12,0)
    \polygon(1,1.5)(1.9,2.85)(10.1,2.85)(11,1.5)
    \polygon(2,3.0)(2.9,4.35)( 9.1,4.35)(10,3.0)
    \polygon(3,4.5)(3.9,5.85)( 8.1,5.85)( 9,4.5)
    \polygon(4,6.0)(4.9,7.35)( 7.1,7.35)( 8,6.0)
    \polygon(5,7.5)(6,9)(7,7.5)

    \put(6,0.67){\makebox(0,0){Personne à personne}}
    \put(6,2.12){\makebox(0,0){Rapports \emph{ad hoc}}}
    \put(6,3.62){\makebox(0,0){Rapports programmés}}
    \put(6,5.12){\makebox(0,0){Apps}}
    \put(6,6.62){\makebox(0,0){Paquetages}}
    \put(6,8.02){\makebox(0,0){API}}

    \put(12,-0,5){\makebox(0,0)[r]{%
        \smaller[2]%
        Adapté de
        \href{https://insurancedatascience.org/downloads/London2023/Mark_Sellors.pdf}{%
          Mark Sellors}}}
  \end{picture}
\end{frame}

\begin{frame}
  \frametitle{Un cours, trois sujets --- et plus}

  \begin{enumerate}
  \item \textbf{Simulation stochastique} \\
    Principes de base de la génération de
    nombres aléatoires uniformes et non uniformes
  \item \textbf{Analyse numérique} \\
    Arithmétique en virgule flottante et méthodes numériques
    classiques
  \item \textbf{Algèbre linéaire} \\
    Rappels du collège, valeurs et
    vecteurs propres
  \item[$+$] \textbf{Méthodes et outils de recherche reproductible} \\
    Programmation lettrée et R~Markdown \\
    Production de rapports dynamiques avec Shiny \\
    Utilisation et conception d'interfaces API
  \end{enumerate}
\end{frame}

\begin{frame}[plain]
  \centering\Large
  \textbf{Avertissement}

  Cours de \alert{mathématiques} \\
  avec applications informatiques
\end{frame}

\begin{frame}[plain]
  \centering\Large
  \textbf{Mes attentes}

  Curiosité \\
  Engagement \\
  Rigueur
\end{frame}

%%% Local Variables:
%%% TeX-master: "methodes-numeriques-en-actuariat"
%%% TeX-engine: xetex
%%% coding: utf-8
%%% End:
