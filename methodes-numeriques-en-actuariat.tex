%%% Copyright (C) 2019-2025 Vincent Goulet
%%%
%%% Ce fichier fait partie du projet «ACT-2002 Méthodes numériques en
%%% actuariat»
%%% https://gitlab.com/vigou3/methodes-numeriques-en-actuariat
%%%
%%% Cette création est mise à disposition sous licence
%%% Attribution-Partage dans les mêmes conditions 4.0
%%% International de Creative Commons.
%%% https://creativecommons.org/licenses/by-sa/4.0/

\documentclass[aspectratio=169,10pt,xcolor=x11names,english,french]{beamer}
  \usepackage{babel}
  \usepackage[autolanguage,np]{numprint}
  \usepackage{amsmath}
  \usepackage[noae]{Sweave}
  \usepackage[mathrm=sym]{unicode-math}  % polices math
  \usepackage{changepage}                % page licence
  \usepackage{tabularx}                  % page licence
  \usepackage{pict2e}                    % pyramide
  \usepackage{relsize}                   % \smaller et al.
  \usepackage{awesomebox}                % \tipbox et autres
  \usepackage{ifsym}                     % symboles LCD
  \usepackage{booktabs}                  % beaux tableaux
  \usepackage{listings}                  % code source
  \usepackage{framed}                    % env. leftbar
  \usepackage[overlay,absolute]{textpos} % covers
  \usepackage{metalogo}                  % logo \XeLaTeX
  \usepackage{icomma}                    % virgule intelligente

  %%% =============================
  %%%  Informations de publication
  %%% =============================
  \title{ACT-2002 Méthodes numériques en actuariat}
  \author{Vincent Goulet}
  \renewcommand{\year}{2025}
  \renewcommand{\month}{01}
  \newcommand{\reposurl}{https://gitlab.com/vigou3/methodes-numeriques-en-actuariat/}

  %%% ===================
  %%%  Style du document
  %%% ===================

  %% Beamer theme
  \usetheme{metropolis}

  %% Polices de caractères
  \setsansfont{Fira Sans Book}
  [
    BoldFont = {Fira Sans SemiBold},
    ItalicFont = {Fira Sans Book Italic},
    BoldItalicFont = {Fira Sans SemiBold Italic}
  ]
  \setmathfont{Fira Math}
  %% -----
  %% Symboles manquants dans Fira Math
  %% https://tex.stackexchange.com/q/540732
  \setmathfont{texgyredejavu-math.otf}[range={\vdots,\ddots}]
  \setmathfont{Fira Math}[range=]
  %% -----
  \newfontfamily\titlefontOS{FiraSans}
  [
    Extension = .otf,
    UprightFont = *-Book,
    BoldFont = *-SemiBold,
    BoldItalicFont = *-SemiBoldItalic,
    Scale = 1.0,
    Numbers = OldStyle
  ]
  \newfontfamily\titlefontFC{FiraSans}
  [
    Extension = .otf,
    UprightFont = *-Book,
    BoldFont = *-SemiBold,
    BoldItalicFont = *-SemiBoldItalic,
    Scale = 1.0,
    Numbers = Uppercase
  ]
  \usepackage[babel=true]{microtype}
  \usepackage{icomma}

  %% Additionnal colors
  \definecolor{comments}{rgb}{0.7,0,0}      % commentaires
  \definecolor{link}{rgb}{0,0.4,0.6}        % internal links
  \definecolor{url}{rgb}{0.6,0,0}           % external links
  \definecolor{codebg}{named}{LightYellow1} % fond code R
  \definecolor{rouge}{rgb}{0.85,0,0.07}     % UL red stripe
  \definecolor{or}{rgb}{1,0.8,0}            % UL yellow stripe
  \definecolor{permitted}{rgb}{0,0.7,0}     % vert permission
  \colorlet{forbidden}{comments}            % rouge interdiction
  \colorlet{alert}{mLightBrown} % alias de couleur Metropolis
  \colorlet{dark}{mDarkTeal}    % alias de couleur Metropolis
  \colorlet{shadecolor}{codebg}

  %% Hyperlinks
  \hypersetup{%
    pdfauthor = {Vincent Goulet},
    pdftitle = {ACT-2002 Méthodes numériques en actuariat},
    colorlinks = {true},
    linktocpage = {true},
    allcolors = {link},
    urlcolor = {url},
    pdfpagemode = {UseOutlines},
    pdfstartview = {Fit}}

  %% Affichage de la table des matières du PDF
  \usepackage{bookmark}
  \bookmarksetup{%
    open = true,
    depth = 3,
    numbered = true}

  %% Paramétrage de babel pour les guillemets
  \frenchbsetup{og=«, fg=»}

  %% Redéfinition des noms de partie changés par Babel pour éviter de
  %% se retrouver avec un titre «Première partie I» sur une page de
  %% partie.
  \addto\captionsfrench{\def\partname{Partie}}

  %% ===============================
  %%  New commands and environments
  %% ===============================

  %% Environnements de Sweave.
  %%
  %% Les environnements Sinput et Soutput utilisent Verbatim (de
  %% fancyvrb). On les réinitialise pour enlever la configuration par
  %% défaut de Sweave, puis on réduit l'écart entre les blocs Sinput
  %% et Soutput.
  \DefineVerbatimEnvironment{Sinput}{Verbatim}{}
  \DefineVerbatimEnvironment{Soutput}{Verbatim}{}
  \fvset{fontsize=\small,listparameters={\setlength{\topsep}{0pt}}}

  %% L'environnement Schunk est complètement redéfini en un hybride
  %% des environnements snugshade* et leftbar de framed.
  \makeatletter
  \renewenvironment{Schunk}{%
    \def\FrameCommand##1{\hskip\@totalleftmargin
      \vrule width 3pt\colorbox{codebg}{\hspace{5pt}##1}%
      % There is no \@totalrightmargin, so:
      \hskip-\linewidth \hskip-\@totalleftmargin \hskip\columnwidth}%
    \MakeFramed {\advance\hsize-\width
      \@totalleftmargin\z@ \linewidth\hsize
      \advance\labelsep\fboxsep
      \@setminipage}%
  }{\par\unskip\@minipagefalse\endMakeFramed}
  \makeatother

  %%% Nouveaux environnements
  \newtheorem*{thm}{Théorème}
  \theoremstyle{definition}
  \newtheorem*{defn}{Définition}
  \newtheorem*{exemple}{Exemple}
  \newtheorem*{rem}{Remarque}
  \newtheorem*{rems}{Remarques}

  %% =====================
  %%  Nouvelles commandes
  %% =====================

  %% Lien externe
  \newcommand{\link}[2]{\href{#1}{#2~{\smaller\faExternalLink*}}}

  %% Noms de fonctions, code, environnement, etc.
  \newcommand{\pkg}[1]{\textbf{#1}}
  \newcommand{\code}[1]{\texttt{#1}}
  \newcommand{\ieee}[3]{\fbox{#1}\hspace{2pt}\fbox{#2}\hspace{2pt}\fbox{#3}}

  %% Identification de la licence CC BY-SA.
  \newcommand{\ccbysa}{\mbox{%
    \faCreativeCommons\kern0.1em%
    \faCreativeCommonsBy\kern0.1em%
    \faCreativeCommonsSa}~\faCopyright[regular]\relax}

  %% Lien vers GitLab dans la page de notices
  \newcommand{\viewsource}[1]{%
    \href{#1}{\faGitlab\ Voir sur GitLab}}

  %% Raccourcis usuels vg
  \newcommand{\esp}[1]{E [ #1 ]}
  \newcommand{\var}[1]{\operatorname{Var} [ #1 ]}
  \newcommand{\mat}[1]{\symbf{#1}}
  \newcommand{\diag}{\operatorname{diag}}
  \newcommand{\R}{\mathbb{R}}

  %%% =======
  %%%  Varia
  %%% =======

  %% Lengths used to compose front and rear covers.
  \newlength{\banderougewidth} \newlength{\banderougeheight}
  \newlength{\bandeorwidth}    \newlength{\bandeorheight}
  \newlength{\imageheight}
  \newlength{\logoheight}

%  \includeonly{presentation}

\begin{document}

%% frontmatter
\include{couverture-avant}
\include{notices}

\begin{frame}
  \frametitle{Sommaire}

  Partie I --- Simulation stochastique \\[6pt]
  \tableofcontents[part=1]

  Partie II --- Analyse numérique \\[6pt]
  \tableofcontents[part=2]

  Partie III --- Algèbre linéaire \\[6pt]
  \tableofcontents[part=3]
\end{frame}


%% mainmatter
\include{presentation}

\part{Simulation stochastique}

\begin{frame}
  \partpage
\end{frame}

\include{simulation}

\part{Analyse numérique}

\begin{frame}
  \partpage
\end{frame}

\include{analyse-numerique}

\part{Algèbre linéaire}

\begin{frame}
  \partpage
\end{frame}

\include{algebre-lineaire}

\bookmarksetup{startatroot}
\include{validation-finale}

%% backmatter
\include{colophon}

\end{document}

%%% Local Variables:
%%% TeX-engine: xetex
%%% TeX-master: t
%%% coding: utf-8
%%% End:
