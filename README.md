<!-- Emacs: -*- coding: utf-8; eval: (auto-fill-mode -1); eval: (visual-line-mode t) -*- -->

# Méthodes numériques en actuariat

Ce projet contient les diapositives des ateliers en classe du cours [ACT-2002 Méthodes numériques en actuariat](https://www.ulaval.ca/les-etudes/cours/repertoire/detailsCours/act-2002-methodes-numeriques-en-actuariat.html) offert par l'[École d'actuariat](https://www.act.ulaval.ca) de l'[Université Laval](https://ulaval.ca).

Le cours de méthodes numériques en actuariat traite de trois sujets principaux ayant comme lien entre eux l'utilisation de l'ordinateur comme outil de calcul: la simulation stochastique, l'analyse numérique et l'algrèbre linéaire. Les objectifs généraux du cours sont:

1. Utiliser la simulation stochastique pour des applications actuarielles.
2. Résoudre de manière efficace des problèmes complexes à l'aide de méthodes numériques.
3. Reconnaitre l'utilité de l'algèbre linéaire en calcul différentiel, en méthodes numériques et en statistique.
4. Se familiariser avec des outils modernes et efficaces de production de rapports dynamiques ou intégrant du texte, des équations mathématiques et des graphiques.

L'ouvrage de référence du cours est [*Méthodes numériques en actuariat avec R*](https://vigou3.gitlab.io/methodes-numeriques-en-actuariat-avec-r).

## Auteur

Vincent Goulet, professeur titulaire, École d'actuariat, Université Laval

## Licence

«Méthodes numériques en actuariat» est mis à disposition sous licence [Attribution-Partage dans les mêmes conditions 4.0 International](https://creativecommons.org/licenses/by-sa/4.0/deed.fr) de Creative Commons.

Consulter le fichier `LICENSE` pour la licence complète.

## Modèle de développement

Le processus de rédaction et de maintenance du projet suit le modèle [*Gitflow Workflow*](https://www.atlassian.com/git/tutorials/comparing-workflows/gitflow-workflow ). Seule particularité: la branche *master* se trouve dans le dépôt [`methodes-numeriques-en-actuariat`](https://gitlab.com/vigou3/methodes-numeriques-en-actuariat) dans GitLab, alors que la branche de développement se trouve dans le dépôt [`methodes-numeriques-en-actuariat-devel`](https://projets.fsg.ulaval.ca/git/scm/vg/methodes-numeriques-en-actuariat-devel) dans le serveur BitBucket de la Faculté des sciences et de génie de l'Université Laval.

Prière de passer par le dépôt `methodes-numeriques-en-actuariat-devel` pour proposer des modifications; consulter le fichier `CONTRIBUTING.md` pour la marche à suivre.

## Obtenir les diapositives

Pour obtenir les diapositives en format PDF, consulter la [page des versions](https://gitlab.com/vigou3/methodes-numeriques-en-actuariat/-/releases) (*releases*).
